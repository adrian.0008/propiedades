import EditEntity from "../utils/EditEntity";
import { urlProperty } from "../utils/endpoints";
import FormProperty from "./FormProperty";
import { propertyCreateDTO, propertyDTO } from "./Model_Property";

export default function EditProperty() {
  return (
    <>
      <EditEntity<propertyCreateDTO, propertyDTO>
        url={urlProperty}
        urlIndex="/property"
        entityName="propiedad."
      >
        {(entity, edit) => (
          <FormProperty
            model={entity}
            onSubmit={async (values) => {
              await edit(values);
            }}
          />
        )}
      </EditEntity>
    </>
  );
}