import { Formik, Form, FormikHelpers, Field, ErrorMessage } from "formik";
import { Link } from "react-router-dom";
import * as Yup from "yup";
import FormGroupText from "../utils/FormGroupText";
import Button from "../utils/Button";
import { propertyCreateDTO } from "./Model_Property";
import { useEffect, useState } from "react";
import { propertyTypeDTO } from "../propertyType/Model_PropertyType";
import { ownerDTO } from "../owner/Model_Owner";
import axios, { AxiosResponse } from "axios";
import { urlOwner, urlPropertyType } from "../utils/endpoints";
import ShowErrorField from "../utils/ShowErrorField";

export default function FormProperty(props: formPropertyProps) {
  const [propertyType, setPropertyType] = useState<propertyTypeDTO[]>([]);
  const [owner, setOwner] = useState<ownerDTO[]>([]);
  
  useEffect(() => {
    axios
      .get(`${urlPropertyType}/loadAll`)
      .then((response: AxiosResponse<propertyTypeDTO[]>) => {
        setPropertyType(response.data);
      });
  },[]);

  useEffect(() => {
    axios
      .get(`${urlOwner}/loadAll`)
      .then((response: AxiosResponse<ownerDTO[]>) => {
        setOwner(response.data);
      });
  },[]);

  return (
    <Formik
      initialValues={props.model}
      onSubmit={props.onSubmit}
      validationSchema={Yup.object({
        propertyTypeId: Yup.number()
          .min(1, "Debe seleccionar un tipo de propiedad.")
          .required("Debe seleccionar un tipo de propiedad."),
        ownerId: Yup.number()
          .min(1, "Debe seleccionar un dueño.")
          .required("Debe seleccionar un dueño."),
        number: Yup.string()
          .required("Este campo es requerido.")
          .max(100, "La longitud máxima es de 100 caracteres."),
        address: Yup.string()
          .required("Este campo es requerido.")
          .max(100, "La longitud máxima es de 100 caracteres."),
        area: Yup.string().required("Este campo es requerido."),
      })}
    >
      {(formikProps) => (
        <Form>
          <div className="row mt-5 mb-2">
            <div className="d-flex justify-content-center">
              <div className="col-12 col-md-10 col-lg-6">
                <div className="card p-sm-3 pb-5 p-2">
                  <div className="mb-3">
                    <div className="input-group">
                      <span className="input-group-text" id="inputGroupPrepend">
                        Tipo de propiedad:
                        <label className="text-danger">*</label>
                      </span>
                      <select
                        {...formikProps.getFieldProps("propertyTypeId")}
                        className="form-select"
                        aria-describedby="inputGroupPrepend"
                      >
                        <option value="0">-- Tipo de propiedad --</option>
                        {propertyType.map((p) => (
                          <option key={p.id} value={p.id}>
                            {p.description}
                          </option>
                        ))}
                      </select>
                    </div>
                    <ErrorMessage name={"propertyTypeId"}>
                      {(message) => <ShowErrorField message={message} />}
                    </ErrorMessage>
                  </div>
                  <div className="mb-3">
                    <div className="input-group">
                      <span className="input-group-text" id="inputGroupPrepend">
                        Dueño:
                        <label className="text-danger">*</label>
                      </span>
                      <select
                        {...formikProps.getFieldProps("ownerId")}
                        className="form-select"
                        aria-describedby="inputGroupPrepend"
                      >
                        <option value="0">-- Dueño --</option>
                        {owner.map((p) => (
                          <option key={p.id} value={p.id}>
                            {p.name}
                          </option>
                        ))}
                      </select>
                    </div>
                    <ErrorMessage name={"ownerId"}>
                      {(message) => <ShowErrorField message={message} />}
                    </ErrorMessage>
                  </div>
                  <FormGroupText
                    campo="number"
                    label="Número:"
                    required={true}
                  />
                  <FormGroupText
                    campo="address"
                    label="Dirección:"
                    required={true}
                  />
                  <FormGroupText campo="area" label="Área:" required={true} />
                  <FormGroupText
                    campo="constructionArea"
                    label="Área de construcción:"
                  />
                  <div className="container mt-3">
                    <div className="row">
                      <div className="col-6 d-grid gap-2">
                        <Link
                          className="btn btn-outline-secondary"
                          to="/property"
                        >
                          Cancelar
                        </Link>
                      </div>
                      <div className="col-6 d-grid gap-2">
                        <Button
                          disabled={formikProps.isSubmitting}
                          type="submit"
                        >
                          Salvar
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
}

interface formPropertyProps {
  model: propertyCreateDTO;
  onSubmit(
    values: propertyCreateDTO,
    accion: FormikHelpers<propertyCreateDTO>
  ): void;
}
