import axios, { AxiosResponse } from "axios";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Charging from "../utils/Charging";
import { urlProperty, urlPropertyType } from "../utils/endpoints";
import {propertyDTO } from "./Model_Property";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { propertyTypeDTO } from "../propertyType/Model_PropertyType";

export default function DetailsProperty() {
  const { id }: any = useParams();
  const [property, setProperty] = useState<propertyDTO>();
  const [propertyType, setPropertyType] = useState<propertyTypeDTO>();

  useEffect(() => {
    axios
      .get(`${urlProperty}/${id}`)
      .then((respuesta: AxiosResponse<propertyDTO>) => {
        setProperty(respuesta.data);
      });
  }, [id]);

  return property ? (
    <div>
      <div className="container mt-4">
        <div className="row">
          <div className="col-2">
            <Link className="btn btn-outline-dark" to="/property">
              <FontAwesomeIcon icon={faArrowLeft} />
            </Link>
          </div>
          <div className="col-10 col-sm-8">
            <h3 className="text-center">Detalle de propiedad.</h3>
          </div>
        </div>
      </div>

      <hr />
      <div className="row mt-5 mb-2">
        <div className="d-flex justify-content-center">
          <div className="col-12 col-md-10 col-lg-8">
            <div className="card p-3 pb-5 p-2">
              <div className="mb-3 row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-3 col-form-label"
                >
                  Tipo de propiedad:
                </label>
                <div className="col-sm-9">
                  <input
                    className="form-control"
                    id="staticEmail"
                    value={property.propertyTypeId}
                    readOnly
                  ></input>
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-3 col-form-label"
                >
                  Dueño:
                </label>
                <div className="col-sm-9">
                  <input
                    className="form-control"
                    id="staticEmail"
                    value={property.ownerId}
                    readOnly
                  ></input>
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-3 col-form-label"
                >
                  Número:
                </label>
                <div className="col-sm-9">
                  <input
                    className="form-control"
                    id="staticEmail"
                    value={property.number}
                    readOnly
                  ></input>
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-3 col-form-label"
                >
                  Dirección:
                </label>
                <div className="col-sm-9">
                  <input
                    className="form-control"
                    id="staticEmail"
                    value={property.address}
                    readOnly
                  ></input>
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-3 col-form-label"
                >
                  Área:
                </label>
                <div className="col-sm-9">
                  <input
                    className="form-control"
                    id="staticEmail"
                    value={property.area}
                    readOnly
                  ></input>
                </div>
              </div>
              <div className="mb-3 row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-3 col-form-label"
                >
                  Área de construcción:
                </label>
                <div className="col-sm-9">
                  <input
                    className="form-control"
                    id="staticEmail"
                    value={property.constructionArea}
                    readOnly
                  ></input>
                </div>
              </div>
              <div className="d-grid gap-2 mt-4 col-8 offset-2 offset-lg-3">
                <Link
                  className="btn btn-secondary mt-3"
                  to={`/property/edit/${property.id}`}
                >
                  Editar
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : (
    <Charging />
  );
}
