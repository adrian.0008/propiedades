import { NumericLiteral } from "typescript";
import { ownerDTO } from "../owner/Model_Owner";
import { propertyTypeDTO } from "../propertyType/Model_PropertyType";

export interface propertyCreateDTO {
    propertyTypeId: number;
    ownerId: number;
    number: string;
    address: string;
    area: number;
    constructionArea: number;
}

export interface propertyDTO{
    id: number;
    propertyTypeId: number;
    ownerId: number;
    number: string;
    address: string;
    area: number;
    constructionArea: number;

    propertyType: propertyTypeDTO;
    owner: ownerDTO;
}

export interface dashboardDTO{
    property: number;
    propertyType: number;
    owner: number;
}