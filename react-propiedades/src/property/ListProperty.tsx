import { urlProperty } from "../utils/endpoints";
import { propertyDTO } from "./Model_Property";
import EntityIndex from "../utils/EntityIndex";

export default function Property() {
  return (
    <>
      <EntityIndex<propertyDTO>
        url={urlProperty}
        urlCreate="property/create"
        title="Listado de propiedades."
        entityName="propiedad"
      >
        {(property, buttons) => (
          <>
            <thead className="table-dark">
              <tr>
                <th>Número</th>
                <th>Dirección</th>
                <th>Área</th>
                <th>Área de construcción</th>
                <th style={{ minWidth: 185 }}>Opciones</th>
              </tr>
            </thead>
            <tbody>
              {property?.map((property) => (
                <tr key={property.id}>
                  <td>{property.number}</td>
                  <td>{property.address}</td>
                  <td>{property.area}</td>
                  <td>{property.constructionArea}</td>
                  <td>
                    {buttons(
                      `property/details/${property.id}`,
                      `property/edit/${property.id}`,
                      property.id
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </>
        )}
      </EntityIndex>
    </>
  );
}
