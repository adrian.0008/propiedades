import axios from "axios";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import { urlProperty } from "../utils/endpoints";
import ShowErrors from "../utils/ShowErrors";
import FormProperty from "./FormProperty";
import { propertyCreateDTO } from "./Model_Property";

export default function CreateProperty() {
  const history = useHistory();
  const [errors, setErrors] = useState<string[]>([]);

  async function create(property: propertyCreateDTO) {
    try {
      const result = await axios.post(urlProperty, property);
      if (result.data == "Registro exitoso.") {
        history.push("/property");
        Swal.fire({
          title: result.data,
          icon: "success",
          position: "top",
          timer: 1000,
          showConfirmButton: false,
        });
      } else {
        Swal.fire({
          title: result.data,
          icon: "error",
          position: "top",
          timer: 2000,
          showConfirmButton: false,
        });
      }
    } catch (error) {
      setErrors(error.response.data);
    }
  }

  return (
    <>
          <h3 className="text-center mt-4">Registro de propiedad.</h3>
      <hr />
      <ShowErrors error={errors} />
      <FormProperty
        model={{ propertyTypeId: 0, ownerId: 0, number: "", address: "", area:0, constructionArea:0 }}
        onSubmit={async (values) => {
          await create(values);
        }}
      />
    </>
  );
}
