import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHouseChimney,
  faHouseCrack,
  faPeopleRoof
} from "@fortawesome/free-solid-svg-icons";
import { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";
import { dashboardDTO } from "./property/Model_Property";
import { urlProperty } from "./utils/endpoints";

export default function LandingPage() {
  const [dashboard, setDashboard] = useState<dashboardDTO>();

  useEffect(() => {
    axios
      .get(`${urlProperty}/count`)
      .then((response: AxiosResponse<dashboardDTO>) => {
        setDashboard(response.data);
      });
  }, []);
    return (
      <>
        <div className="container-fluid px-4 my-3">
          <div className="row g-4">
            <div className="col-sm-6 col-xl-4">
              <div className="bg-light rounded d-flex align-items-center justify-content-between p-4">
                <FontAwesomeIcon icon={faHouseCrack} className="fa-3x" />
                <div className="ms-3">
                  <p className="mb-2">Tipo de propiedades</p>
                  <h6 className="mb-0 text-center">{dashboard?.propertyType}</h6>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-xl-4">
              <div className="bg-light rounded d-flex align-items-center justify-content-between p-4">
                <FontAwesomeIcon icon={faPeopleRoof} className="fa-3x" />
                <div className="ms-3">
                  <p className="mb-2">Dueños</p>
                  <h6 className="mb-0 text-center">{dashboard?.owner}</h6>
                </div>
              </div>
            </div>
            <div className="col-sm-6 col-xl-4">
              <div className="bg-light rounded d-flex align-items-center justify-content-between p-4">
                <FontAwesomeIcon icon={faHouseChimney} className="fa-3x" />
                <div className="ms-3">
                  <p className="mb-2">Propiedades</p>
                  <h6 className="mb-0 text-center">{dashboard?.property}</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
}