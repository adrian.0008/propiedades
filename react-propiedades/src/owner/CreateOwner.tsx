import axios from "axios";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import { urlOwner } from "../utils/endpoints";
import ShowErrors from "../utils/ShowErrors";
import FormOwner from "./FormOwner";
import { ownerCreateDTO } from "./Model_Owner";

export default function CreateOwner() {
  const history = useHistory();
  const [errors, setErrors] = useState<string[]>([]);

  async function create(owner: ownerCreateDTO) {
    try {
      console.log(owner);
      const result = await axios.post(urlOwner, owner);
      if (result.data == "Registro exitoso.") {
        history.push("/owner");
        Swal.fire({
          title: result.data,
          icon: "success",
          position: "top",
          timer: 1000,
          showConfirmButton: false,
        });
      } else {
        Swal.fire({
          title: result.data,
          icon: "error",
          position: "top",
          timer: 2000,
          showConfirmButton: false,
        });
      }
    } catch (error) {
      setErrors(error.response.data);
    }
  }

  return (
    <>
      <h3 className="text-center mt-4">Registro de dueños.</h3>
      <hr />
      <ShowErrors error={errors} />
      <FormOwner
        model={{
          identificationNumber: "",
          name: "",
          email: "",
          telephone: "",
          address: "",
        }}
        onSubmit={async (values) => {
          await create(values);
        }}
      />
    </>
  );
}
