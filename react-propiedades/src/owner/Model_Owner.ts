export interface ownerCreateDTO {
    name: string;
    telephone: string;
    email: string;
    identificationNumber: string;
    address: string;
}

export interface ownerDTO{
    id: number;
    name: string;
    telephone: string;
    email: string;
    identificationNumber: string;
    address: string;
}