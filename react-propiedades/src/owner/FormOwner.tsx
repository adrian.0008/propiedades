import { Formik, Form, FormikHelpers } from "formik";
import { Link } from "react-router-dom";
import * as Yup from "yup";
import FormGroupText from "../utils/FormGroupText";
import Button from "../utils/Button";
import { ownerCreateDTO } from "./Model_Owner";

export default function FormOwner(props: formOwnerProps) {
  return (
    <Formik
      initialValues={props.model}
      onSubmit={props.onSubmit}
      validationSchema={Yup.object({
        identificationNumber: Yup.string()
          .required("Este campo es requerido.")
          .max(12, "La longitud máxima es de 12 caracteres"),
        name: Yup.string()
          .required("Este campo es requerido.")
          .max(50, "La longitud máxima es de 50 caracteres"),
        telephone: Yup.string()
          .required("Este campo es requerido.")
          .min(8, "La longitud mínima es de 8 caracteres")
          .max(12, "La longitud máxima es de 255 caracteres"),
        email: Yup.string()
          .email("Debe colocar un email válido.")
          .max(50, "La longitud máxima es de 50 caracteres"),
        address: Yup.string().max(50, "La longitud máxima es de 50 caracteres"),
      })}
    >
      {(formikProps) => (
        <Form>
          <div className="row mt-5 mb-2">
            <div className="d-flex justify-content-center">
              <div className="col-12 col-md-10 col-lg-6">
                <div className="card p-sm-3 pb-5 p-2">
                  <FormGroupText
                    campo="identificationNumber"
                    label="Cédula:"
                    required={true}
                  />
                  <FormGroupText campo="name" label="Nombre:" required={true} />
                  <FormGroupText
                    campo="telephone"
                    label="Teléfono:"
                    required={true}
                  />
                  <FormGroupText campo="email" label="Correo:" />
                  <FormGroupText campo="address" label="Dirección:" />
                  <div className="container mt-3">
                    <div className="row">
                      <div className="col-6 d-grid gap-2">
                        <Link className="btn btn-outline-secondary" to="/owner">
                          Cancelar
                        </Link>
                      </div>
                      <div className="col-6 d-grid gap-2">
                        <Button
                          disabled={formikProps.isSubmitting}
                          type="submit"
                        >
                          Salvar
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
}

interface formOwnerProps {
  model: ownerCreateDTO;
  onSubmit(
    values: ownerCreateDTO,
    accion: FormikHelpers<ownerCreateDTO>
  ): void;
}
