import EditEntity from "../utils/EditEntity";
import { urlOwner } from "../utils/endpoints";
import FormOwner from "./FormOwner";
import { ownerCreateDTO, ownerDTO } from "./Model_Owner";

export default function EditOwner() {
  return (
    <>
      <EditEntity<ownerCreateDTO, ownerDTO>
        url={urlOwner}
        urlIndex="/owner"
        entityName="dueño."
      >
        {(entity, edit) => (
          <FormOwner
            model={entity}
            onSubmit={async (values) => {
              await edit(values);
            }}
          />
        )}
      </EditEntity>
    </>
  );
}
