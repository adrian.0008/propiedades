import { urlOwner } from "../utils/endpoints";
import { ownerDTO } from "./Model_Owner";
import EntityIndex from "../utils/EntityIndex";

export default function Owner() {
  return (
    <>
      <EntityIndex<ownerDTO>
        url={urlOwner}
        urlCreate="owner/create"
        title="Listado de dueños."
        entityName="dueño"
      >
        {(owner, buttons) => (
          <>
            <thead className="table-dark">
              <tr>
                <th>Cédula</th>
                <th>Nombre</th>
                <th>Email</th>
                <th style={{minWidth: 185}}>Opciones</th>
              </tr>
            </thead>
            <tbody>
              {owner?.map((owner) => (
                <tr key={owner.id}>
                  <td>{owner.identificationNumber}</td>
                  <td>{owner.name}</td>
                  <td>{owner.email}</td>
                  <td>
                    {buttons(`owner/details/${owner.id}`, `owner/edit/${owner.id}`, owner.id)}
                  </td>
                </tr>
              ))}
            </tbody>
          </>
        )}
      </EntityIndex>
    </>
  );
}
