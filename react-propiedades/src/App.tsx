import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import routes from './route-config';
import Menu from './utils/Menu';

function App() {
  return (
    <>
      <BrowserRouter>
        <Menu />
        <div className="container-xl" id="menu">
          <Switch>
            {routes.map(routes => 
            <Route key={routes.path} path={routes.path}
              exact={routes.exact}>
                <routes.component />
              </Route>)}
          </Switch>
        </div>
      </BrowserRouter>
    </>
  );
}

export default App;
