const apiURL = process.env.REACT_APP_API_URL;

export const urlPropertyType = `${apiURL}/propertyType`
export const urlOwner = `${apiURL}/owner`
export const urlProperty = `${apiURL}/property`