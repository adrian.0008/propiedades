export default function ShowErrorField(props: showErrorFieldProps){
    return (
        <div className="text-danger text-center">{props.message}</div>
    )
}

interface showErrorFieldProps{
    message: string;
}