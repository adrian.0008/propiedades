export default function ShowErrors(props: showErrorsProps){
    const style= {color: 'red'}
    return(
        <>
            {props.error ? <ul style={style}>
                {props.error.map((bug, index) => <li key={index}>{bug}</li>)}
            </ul> : null}
        </>
    )
}

interface showErrorsProps{
    error?: string[];
}