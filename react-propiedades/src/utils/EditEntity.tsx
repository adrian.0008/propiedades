import axios, { AxiosResponse } from "axios";
import { useState, useEffect, ReactElement } from "react";
import { useParams, useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import ShowErrors from "./../utils/ShowErrors";
import Charging from "./Charging";

export default function EditEntity<TCreation, TReading>(
  props: editEntityProps<TCreation, TReading>) {
  const { id }: any = useParams();
  const [entity, setEntity] = useState<TCreation>();
  const [errors, setErrors] = useState<string[]>([]);
  const history = useHistory();

  useEffect(() => {
    axios
      .get(`${props.url}/${id}`)
      .then((respuesta: AxiosResponse<TReading>) => {
        setEntity(props.transform(respuesta.data));
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  async function edit(EditEntity: TCreation) {
    try {
        const result = await axios.put(`${props.url}/${id}`, EditEntity);
        if (result.data === "Registro exitoso.") {
          history.push(props.urlIndex);
          Swal.fire({
            title: result.data,
            icon: "success",
            position: "top",
            timer: 1000,
            showConfirmButton: false,
          });
        } else {
          Swal.fire({
            title: result.data,
            icon: "error",
            position: "top",
            timer: 2000,
            showConfirmButton: false,
          });
        }
    } catch (error) {
      setErrors(error.response.data);
    }
  }

  return (
    <>
      <h3 className="text-center mt-4">Editar {props.entityName}</h3>
      <hr />
      <ShowErrors error={errors} />
      {entity ? props.children(entity, edit) : <Charging />}
    </>
  );
}

interface editEntityProps<TCreation, TReading> {
  url: string;
  urlIndex: string;
  entityName: string;
  children(entity: TCreation, edit: (entity: TCreation) => void): ReactElement;
  transform(entidad: TReading): TCreation;
  transformFormData?(model: TCreation): FormData;
}

EditEntity.defaultProps = {
  transform: (entity: any) => entity,
};
