import { useContext, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHouseChimney,
  faBars,
  faHouseCrack,
  faPeopleRoof
} from "@fortawesome/free-solid-svg-icons";
import axios, { AxiosResponse } from "axios";

export default function Menu() {
  const activeClass = "active";
  
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <div className="container-fluid">
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target=".navbar-collapse"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto">
            <li className="nav-item ms-0 ms-lg-2">
              <NavLink
                className="nav-link"
                activeClassName={activeClass}
                to="/"
              >
                <FontAwesomeIcon icon={faBars} /> Inicio
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                className="nav-link"
                activeClassName={activeClass}
                to="/propertyType"
              >
                <FontAwesomeIcon icon={faHouseCrack} /> Tipo de propiedad
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                className="nav-link"
                activeClassName={activeClass}
                to="/owner"
              >
                <FontAwesomeIcon icon={faPeopleRoof} /> Dueños
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                className="nav-link"
                activeClassName={activeClass}
                to="/property"
              >
                <FontAwesomeIcon icon={faHouseChimney} /> Propiedades
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
