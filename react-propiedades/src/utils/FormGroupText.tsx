import { Field, ErrorMessage } from "formik";
import ShowErrorField from "./ShowErrorField";

export default function FormGroupText(props: formGroupTextProps) {
  return (
    <div className="mb-3">
      <div className="input-group">
        <span className="input-group-text" id="inputGroupPrepend">
          {props.label}{props.required === true ? (<label className="text-danger">*</label>):null}
        </span>
        <Field
          name={props.campo}
          className="form-control"
          placeholder={props.placeholder}
          type={props.type}
          aria-describedby="inputGroupPrepend"
        />
      </div>
      <ErrorMessage name={props.campo}>
        {(message) => <ShowErrorField message={message} />}
      </ErrorMessage>
    </div>
  );
}

interface formGroupTextProps {
  campo: string;
  label?: string;
  placeholder?: string;
  type?: string;
  required?: boolean;
}
