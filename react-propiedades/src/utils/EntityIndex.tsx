import axios, { AxiosResponse } from "axios";
import { ReactElement, useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Button from "./Button";
import confirm from "./Confirm";
import Pagination from "./Pagination";
import GenericList from "./GenericList";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMagnifyingGlass,
  faPenToSquare,
  faTrashCan,
  faBars,
  faPlus,
} from "@fortawesome/free-solid-svg-icons";
import Swal from "sweetalert2";

export default function EntityIndex<T>(props: entityIndexProps<T>) {
  const [entity, setEntity] = useState<T[]>();
  const [totalDepages, setTotalDepages] = useState(0);
  const [recordsByPage, setRecordsByPage] = useState(10);
  const [page, setPage] = useState(1);
  const [idParish] = useState(props.idParish);

  useEffect(() => {
    loadData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page, recordsByPage]);

  function loadData() {
    if (props.idParish == null) {
      axios
        .get(props.url, {
          params: { page, recordsByPage },
        })
        .then((respuesta: AxiosResponse<T[]>) => {
          const totalDeRegistros = parseInt(
            respuesta.headers["totalnumberofrecords"],
            10
          );
          setTotalDepages(Math.ceil(totalDeRegistros / recordsByPage));
          setEntity(respuesta.data);
          //console.log(respuesta.data);
        });
    } else {
      axios
        .get(props.url, {
          params: { page, recordsByPage, idParish },
        })
        .then((respuesta: AxiosResponse<T[]>) => {
          const totalDeRegistros = parseInt(
            respuesta.headers["totalnumberofrecords"],
            10
          );
          setTotalDepages(Math.ceil(totalDeRegistros / recordsByPage));
          setEntity(respuesta.data);
          //console.log(respuesta.data);
        });
    }
  }

  async function erase(id: number) {
    try {
      const result = await axios.delete(`${props.url}/${id}`);
      if(result.data === "Registro exitoso."){
        if(page > 1 && entity?.length === 1){
          setPage(page - 1);
        }
        loadData();
        Swal.fire({
          title: result.data,
          icon: "success",
          position: "top",
          timer: 1000,
          showConfirmButton: false,
        });
      }else{
        Swal.fire({
          title: result.data,
          icon: "error",
          position: "top",
          timer: 2000,
          showConfirmButton: false,
        });
      }
    } catch (error) {
      console.log(error.response.data);
    }
  }

  const buttons = (
    urlDetails: string,
    urlEdit: string,
    id: number
  ) => (
    <>
      <Link className="btn btn-outline-info" title="Ver fila" to={urlDetails}>
        <FontAwesomeIcon icon={faMagnifyingGlass} />
      </Link>
        <Link
          className="btn btn-outline-success mx-1"
          title="Editar fila"
          to={urlEdit}
        >
          <FontAwesomeIcon icon={faPenToSquare} />
        </Link>
        <Button
          title="Eliminar fila"
          onClick={() => confirm(() => erase(id))}
          className="btn btn-outline-danger"
        >
          <FontAwesomeIcon icon={faTrashCan} />
        </Button>
    </>
  );

  return (
    <>
      <div className="container mt-4 mb-4">
        <div className="d-flex justify-content-between">
          <Link className="btn btn-outline-dark" to="/" title="Inicio">
            <FontAwesomeIcon icon={faBars} />
          </Link>

          <h3 className="text-center smallTitle">{props.title}</h3>
          <Link
            className="btn btn-outline-dark"
            to={props.urlCreate}
            title="Nuevo"
          >
            <FontAwesomeIcon icon={faPlus} />
          </Link>
        </div>
      </div>

      <GenericList list={entity}>
        <div className="table-responsive">
          <table className="table table-striped table-hover">
            {props.children(entity!, buttons)}
          </table>
        </div>
      </GenericList>

      <Pagination
        totalNumberOfPages={totalDepages}
        actualPage={page}
        onChange={(newPage) => setPage(newPage)}
      />
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-auto">
            <div className="form-floating px-2">
              <select
                className="form-select text-center"
                id="floatingSelect"
                aria-label="Floating label select example"
                defaultValue={10}
                onChange={(e) => {
                  setPage(1);
                  setRecordsByPage(parseInt(e.currentTarget.value, 10));
                }}
              >
                <option value={5}>5</option>
                <option value={10}>10</option>
                <option value={25}>25</option>
                <option value={50}>50</option>
              </select>
              <label htmlFor="floatingSelect">Mostrar: </label>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

interface entityIndexProps<T> {
  url: string;
  urlCreate: string;
  children(
    entity: T[],
    buttons: (
      urlDetails: string,
      urlEdit: string,
      id: number,
      state?: number
    ) => ReactElement
  ): ReactElement;
  title: string;
  entityName: string;
  idParish?: number;
}
