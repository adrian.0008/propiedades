import { useEffect, useState } from "react";

export default function Pagination(props: paginationProps) {
    const [listLinks, setListLinks] = useState<modeloLink[]>([]);
    useEffect(() => {
        const previousPageEnabled = props.actualPage !== 1;
        const previousPage = props.actualPage - 1;
        const links: modeloLink[] = [];

        links.push({
            text: 'Anterior',
            enabled: previousPageEnabled,
            page: previousPage,
            active: false
        });

        for (let i = 1; i <= props.totalNumberOfPages; i++) {
            if (i >= props.actualPage - props.radio && i <= props.actualPage + props.radio) {
                links.push({
                    text: `${i}`,
                    active: props.actualPage === i,
                    enabled: true, page: i
                })
            }
        }

        const nextPageEnabled = props.actualPage !== props.totalNumberOfPages && props.totalNumberOfPages > 0;
        const nextPage = props.actualPage + 1;
        links.push({
            text: 'Siguiente',
            page: nextPage,
            enabled: nextPageEnabled,
            active: false
        });

        setListLinks(links);
    }, [props.actualPage, props.totalNumberOfPages, props.radio])

    function getClass(link: modeloLink){
        if (link.active){
            return "active pointer"
        }

        if (!link.enabled){
            return "disabled";
        }

        return "pointer"
    }

    function selectPage(link: modeloLink){
        if (link.page === props.actualPage){
            return;
        }

        if (!link.enabled){
            return;
        }

        props.onChange(link.page);
    }

    return (
        <nav>
            <ul className="pagination justify-content-center mt-3">
                {listLinks.map(link => <li key={link.text}
                 onClick={() => selectPage(link)}
                 className={`page-item cursor ${getClass(link)}`}
                >
                    <span className="page-link">{link.text}</span>
                </li>)}
            </ul>
        </nav>
    )
}

interface modeloLink {
    page: number;
    enabled: boolean;
    text: string;
    active: boolean;
}

interface paginationProps {
    actualPage: number;
    totalNumberOfPages: number;
    radio: number;
    onChange(page: number): void;
}

Pagination.defaultProps = {
    radio: 3
}
