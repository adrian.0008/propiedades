import { ReactElement } from "react";
import Charging from "./Charging";

export default function GenericList(props: genericListProps){
    if (!props.list){
        if (props.loadUI){
            return props.loadUI;
        }
        return <Charging />
    } else if (props.list.length === 0){
        if (props.emptyListUI){
            return props.emptyListUI;
        }
        return <><div className="text-center m-5">No hay elementos para mostrar.</div></>
    } else{
        return props.children;
    }
}

interface genericListProps{
    list: any;
    children: ReactElement;
    loadUI?: ReactElement;
    emptyListUI?: ReactElement;
}