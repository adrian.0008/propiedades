import LandingPage from "./LandingPage";
import RedirectToLanding from "./utils/RedirectToLanding";

import CreatePropertyType from "./propertyType/CreatePropertyType";
import DetailsPropertyType from "./propertyType/DetailsPropertyType";
import EditPropertyType from "./propertyType/EditPropertyType";
import ListPropertyType from "./propertyType/ListPropertyType";

import CreateOwner from "./owner/CreateOwner";
import DetailsOwner from "./owner/DetailsOwner";
import EditOwner from "./owner/EditOwner";
import ListOwner from "./owner/ListOwner";

import CreateProperty from "./property/CreateProperty";
import DetailsProperty from "./property/DetailsProperty";
import EditProperty from "./property/EditProperty";
import ListProperty from "./property/ListProperty";

const routes = [
    // Tipos de propiedad,
    { path: "/propertyType/create", component: CreatePropertyType },
    { path: "/propertyType/details/:id(\\d+)", component: DetailsPropertyType },
    { path: "/propertyType/edit/:id(\\d+)", component: EditPropertyType },
    { path: "/propertyType", component: ListPropertyType, exact: true},
    // Owners,
    { path: "/owner/create", component: CreateOwner },
    { path: "/owner/details/:id(\\d+)", component: DetailsOwner },
    { path: "/owner/edit/:id(\\d+)", component: EditOwner },
    { path: "/owner", component: ListOwner, exact: true},
    // Propiedades,
    { path: "/property/create", component: CreateProperty },
    { path: "/property/details/:id(\\d+)", component: DetailsProperty },
    { path: "/property/edit/:id(\\d+)", component: EditProperty },
    { path: "/property", component: ListProperty, exact: true},

    { path: "/", component: LandingPage, exact: true},
    { path: "*", component: RedirectToLanding },
];

export default routes;