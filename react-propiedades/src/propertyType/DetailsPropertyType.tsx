import axios, { AxiosResponse } from "axios";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Charging from "../utils/Charging";
import { urlPropertyType } from "../utils/endpoints";
import {propertyTypeDTO } from "./Model_PropertyType";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";

export default function DetailsPropertyType() {
  const { id }: any = useParams();
  const [propertyType, setPropertyType] = useState<propertyTypeDTO>();

  useEffect(() => {
    axios
      .get(`${urlPropertyType}/${id}`)
      .then((respuesta: AxiosResponse<propertyTypeDTO>) => {
        setPropertyType(respuesta.data);
      });
  }, [id]);

  return propertyType ? (
    <div>
      <div className="container mt-4">
        <div className="row">
          <div className="col-2">
            <Link className="btn btn-outline-dark" to="/propertyType">
              <FontAwesomeIcon icon={faArrowLeft} />
            </Link>
          </div>
          <div className="col-10 col-sm-8">
            <h3 className="text-center">Detalle de tipo de propiedad.</h3>
          </div>
        </div>
      </div>

      <hr />
      <div className="row mt-5 mb-2">
        <div className="d-flex justify-content-center">
          <div className="col-12 col-md-10 col-lg-6">
            <div className="card p-3 pb-5 p-2">
              <div className="mb-3 row">
                <label
                  htmlFor="staticEmail"
                  className="col-sm-3 col-form-label"
                >
                  Descripción:
                </label>
                <div className="col-sm-9">
                  <input
                    className="form-control"
                    id="staticEmail"
                    value={propertyType.description}
                    readOnly
                  ></input>
                </div>
              </div>
              <div className="d-grid gap-2 mt-4 col-8 offset-2 offset-lg-3">
                <Link
                  className="btn btn-secondary mt-3"
                  to={`/propertyType/edit/${propertyType.id}`}
                >
                  Editar
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : (
    <Charging />
  );
}
