import { urlPropertyType } from "../utils/endpoints";
import { propertyTypeDTO } from "./Model_PropertyType";
import EntityIndex from "../utils/EntityIndex";

export default function PropertyType() {
  return (
    <>
      <EntityIndex<propertyTypeDTO>
        url={urlPropertyType}
        urlCreate="propertyType/create"
        title="Listado de tipos de propiedades."
        entityName="tipo de propiedad"
      >
        {(propertyType, buttons) => (
          <>
            <thead className="table-dark">
              <tr>
                <th>Descripción</th>
                <th style={{minWidth: 185}}>Opciones</th>
              </tr>
            </thead>
            <tbody>
              {propertyType?.map((propertyType) => (
                <tr key={propertyType.id}>
                  <td>{propertyType.description}</td>
                  <td>
                    {buttons(`propertyType/details/${propertyType.id}`, `propertyType/edit/${propertyType.id}`, propertyType.id)}
                  </td>
                </tr>
              ))}
            </tbody>
          </>
        )}
      </EntityIndex>
    </>
  );
}
