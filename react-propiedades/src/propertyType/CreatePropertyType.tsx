import axios from "axios";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import { urlPropertyType } from "../utils/endpoints";
import ShowErrors from "../utils/ShowErrors";
import FormPropertyType from "./FormPropertyType";
import { propertyTypeCreateDTO } from "./Model_PropertyType";

export default function CreatePropertyType() {
  const history = useHistory();
  const [errors, setErrors] = useState<string[]>([]);

  async function create(propertyType: propertyTypeCreateDTO) {
    try {
      const result = await axios.post(urlPropertyType, propertyType);
      if (result.data == "Registro exitoso.") {
        history.push("/propertyType");
        Swal.fire({
          title: result.data,
          icon: "success",
          position: "top",
          timer: 1000,
          showConfirmButton: false,
        });
      } else {
        Swal.fire({
          title: result.data,
          icon: "error",
          position: "top",
          timer: 2000,
          showConfirmButton: false,
        });
      }
    } catch (error) {
      setErrors(error.response.data);
    }
  }

  return (
    <>
          <h3 className="text-center mt-4">Registro de tipo de propiedad.</h3>
      <hr />
      <ShowErrors error={errors} />
      <FormPropertyType
        model={{ description: "" }}
        onSubmit={async (values) => {
          await create(values);
        }}
      />
    </>
  );
}
