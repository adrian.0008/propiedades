export interface propertyTypeCreateDTO {
    description: string;
}

export interface propertyTypeDTO{
    id: number;
    description: string;
}