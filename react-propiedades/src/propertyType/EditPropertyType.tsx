import EditEntity from "../utils/EditEntity";
import { urlPropertyType } from "../utils/endpoints";
import FormPropertyType from "./FormPropertyType";
import { propertyTypeCreateDTO, propertyTypeDTO } from "./Model_PropertyType";

export default function EditOwner() {
  return (
    <>
      <EditEntity<propertyTypeCreateDTO, propertyTypeDTO>
        url={urlPropertyType}
        urlIndex="/propertyType"
        entityName="tipo de propiedad."
      >
        {(entity, edit) => (
          
          <FormPropertyType          
            model={entity}
            onSubmit={async (values) => {
              await edit(values);
            }}
          />
        )}
      </EditEntity>
    </>
  );
}
