import { Formik, Form, FormikHelpers, Field } from "formik";
import { useContext } from "react";
import { Link } from "react-router-dom";
import * as Yup from "yup";
import FormGroupText from "../utils/FormGroupText";
import Button from "../utils/Button";
import { propertyTypeCreateDTO } from "./Model_PropertyType";

export default function FormPropertyType(props: formPropertyTypeProps) {
  
  return (
    (
      <Formik
        initialValues={props.model}
        onSubmit={props.onSubmit}
        validationSchema={Yup.object({
          description: Yup.string()
            .required("Este campo es requerido.")
            .max(150, "La longitud máxima es de 150 caracteres"),
        })}
      >
        {(formikProps) => (
          <Form>
            <div className="row mt-5 mb-2">
              <div className="d-flex justify-content-center">
                <div className="col-12 col-md-10 col-lg-6">
                  <div className="card p-sm-3 pb-5 p-2">
                    <FormGroupText campo="description" label="Descripción:" required={true}/>
                    <div className="container mt-3">
                      <div className="row">
                        <div className="col-6 d-grid gap-2">
                          <Link
                            className="btn btn-outline-secondary"
                            to="/propertyType"
                          >
                            Cancelar
                          </Link>
                        </div>
                        <div className="col-6 d-grid gap-2">
                          <Button
                            disabled={formikProps.isSubmitting}
                            type="submit"
                          >
                            Salvar
                          </Button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    )
  );
}

interface formPropertyTypeProps {
  model: propertyTypeCreateDTO;
  onSubmit(
    values: propertyTypeCreateDTO,
    accion: FormikHelpers<propertyTypeCreateDTO>
  ): void;
}
