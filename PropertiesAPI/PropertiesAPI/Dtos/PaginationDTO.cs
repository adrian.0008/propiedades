﻿namespace PropertiesAPI.Dtos
{
    public class PaginationDTO
    {
        public int Page { get; set; } = 1;
        private int recordsByPage = 10;
        private readonly int cantidadMaximaRecordsPorPagina = 50;

        public int RecordsByPage
        {
            get
            {
                return recordsByPage;
            }
            set
            {
                recordsByPage = (value > cantidadMaximaRecordsPorPagina) ? cantidadMaximaRecordsPorPagina : value;
            }
        }
    }
}
