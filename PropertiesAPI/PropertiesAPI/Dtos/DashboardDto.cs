﻿namespace PropertiesAPI.Dtos
{
    public class DashboardDto
    {
        public int property { get; set; }
        public int propertyType { get; set; }
        public int owner { get; set; }
    }
}
