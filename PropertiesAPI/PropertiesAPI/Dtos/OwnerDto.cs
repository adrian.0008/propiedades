﻿using System;
using System.Collections.Generic;

namespace PropertiesAPI.Dtos
{
    public partial class OwnerDto
    {
        public OwnerDto()
        {
            Properties = new HashSet<PropertyDto>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Telephone { get; set; } = null!;
        public string? Email { get; set; }
        public string IdentificationNumber { get; set; } = null!;
        public string? Address { get; set; }

        public virtual ICollection<PropertyDto> Properties { get; set; }
    }
}
