﻿using System;
using System.Collections.Generic;

namespace PropertiesAPI.Dtos
{
    public partial class PropertyTypeDto
    {
        public PropertyTypeDto()
        {
            Properties = new HashSet<PropertyDto>();
        }

        public int Id { get; set; }
        public string Description { get; set; } = null!;

        public virtual ICollection<PropertyDto> Properties { get; set; }
    }
}
