﻿using System;
using System.Collections.Generic;

namespace PropertiesAPI.Dtos
{
    public partial class PropertyDto
    {
        public int Id { get; set; }
        public int? PropertyTypeId { get; set; }
        public int? OwnerId { get; set; }
        public string Number { get; set; } = null!;
        public string Address { get; set; } = null!;
        public decimal Area { get; set; }
        public decimal? ConstructionArea { get; set; }

        public virtual OwnerDto? Owner { get; set; }
        public virtual PropertyTypeDto? PropertyType { get; set; }
    }
}
