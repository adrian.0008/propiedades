﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PropertiesAPI.Dtos;
using PropertiesAPI.Models;
using PropertiesAPI.Utilities;

namespace PropertiesAPI.Controllers
{
    [ApiController]
    [Route("api/owner")]
    public class OwnerController : ControllerBase
    {

        private readonly ILogger<OwnerController> logger;
        private readonly PropertiesContext context;
        private readonly IMapper mapper;

        public OwnerController(
            ILogger<OwnerController> logger,
            PropertiesContext context,
            IMapper mapper)
        {
            this.logger = logger;
            this.context = context;
            this.mapper = mapper;
        }
        // This method loads the list of the requested model by pages.
        [HttpGet]
        public async Task<ActionResult<List<OwnerDto>>> Get([FromQuery] PaginationDTO paginationDTO)
        {
            var queryable = context.Owners.AsQueryable(); ;
            await HttpContext.InsertPaginationParametersInHeader(queryable);
            var owner = await queryable.OrderBy(x => x.Name).Paginate(paginationDTO).ToListAsync();
            return mapper.Map<List<OwnerDto>>(owner);
        }
        // This method loads the list of the requested the all owner.
        [HttpGet("loadAll")]
        public async Task<ActionResult<List<OwnerDto>>> LoadAll()
        {
            var owner = await context.Owners.OrderBy(x => x.Name).ToListAsync();
            return mapper.Map<List<OwnerDto>>(owner);
        }

        // This method loads all the information of the selected owner.
        [HttpGet("{Id:int}")]
        public async Task<ActionResult<OwnerDto>> Get(int Id)
        {
            var owner = await context.Owners.FirstOrDefaultAsync(x => x.Id == Id);
            if (owner == null)
            {
                return NotFound();
            }
            return mapper.Map<OwnerDto>(owner);
        }

        // This method saves a new owner.
        [HttpPost]
        public async Task<ActionResult<string>> Post([FromBody] OwnerDto ownerDto)
        {
            try
            {
                var owner = mapper.Map<Owner>(ownerDto);
                context.Add(owner);
                await context.SaveChangesAsync();
                return "Registro exitoso.";
            }
            catch (Exception ex)
            {
                return "Ocurrió un error.";
            }
        }

        // This method edits the selected owner.
        [HttpPut("{Id:int}")]
        public async Task<ActionResult<string>> Put(int Id, [FromBody] OwnerDto ownerDto)
        {
            try
            {
                var owner = await context.Owners.FirstOrDefaultAsync(x => x.Id == Id);
                if (owner == null)
                {
                    return "No existe este dueño.";
                }
                owner = mapper.Map(ownerDto, owner);
                await context.SaveChangesAsync();
                return "Registro exitoso.";
            }
            catch (Exception ex)
            {
                return "Ocurrió un error.";
            }
        }

        // Delete this record from the database.
        [HttpDelete("{Id:int}")]
        public async Task<ActionResult<string>> Delete(int Id)
        {
            try
            {
                var owner = await context.Owners.FirstOrDefaultAsync(x => x.Id == Id);
                if (owner == null)
                {
                    return "No se pudo eliminar, no se encontró este dueño.";
                }
                context.Remove(owner);
                await context.SaveChangesAsync();
                return "Registro exitoso.";
            }
            catch (Exception ex)
            {
                return "Ocurrió un error.";
            }
        }
    }
}
