﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PropertiesAPI.Dtos;
using PropertiesAPI.Models;
using PropertiesAPI.Utilities;

namespace PropertiesAPI.Controllers
{
    [ApiController]
    [Route("api/property")]
    public class PropertyController : ControllerBase
    {
        private readonly ILogger<PropertyController> logger;
        private readonly PropertiesContext context;
        private readonly IMapper mapper;

        public PropertyController(
            ILogger<PropertyController> logger,
            PropertiesContext context,
            IMapper mapper)
        {
            this.logger = logger;
            this.context = context;
            this.mapper = mapper;
        }
        // This method loads the list of the requested model by pages.
        [HttpGet]
        public async Task<ActionResult<List<PropertyDto>>> Get([FromQuery] PaginationDTO paginationDTO)
        {
            var queryable = context.Properties.AsQueryable(); ;
            await HttpContext.InsertPaginationParametersInHeader(queryable);
            var property = await queryable.OrderBy(x => x.Id).Paginate(paginationDTO).ToListAsync();
            return mapper.Map<List<PropertyDto>>(property);
        }

        // This method loads all the information of the selected property.
        [HttpGet("{Id:int}")]
        public async Task<ActionResult<PropertyDto>> Get(int Id)
        {
            var property = await context.Properties.FirstOrDefaultAsync(x => x.Id == Id);
            if (property == null)
            {
                return NotFound();
            }
            return mapper.Map<PropertyDto>(property);
        }

        // This method saves a new property.
        [HttpPost]
        public async Task<ActionResult<string>> Post([FromBody] PropertyDto propertyDto)
        {
            try
            {
                var property = mapper.Map<Property>(propertyDto);
                context.Add(property);
                await context.SaveChangesAsync();
                return "Registro exitoso.";
            }
            catch (Exception ex)
            {
                return "Ocurrió un error.";
            }
        }

        // This method edits the selected property.
        [HttpPut("{Id:int}")]
        public async Task<ActionResult<string>> Put(int Id, [FromBody] PropertyDto propertyDto)
        {
            try
            {
                var property = await context.Properties.FirstOrDefaultAsync(x => x.Id == Id);
                if (property == null)
                {
                    return "No existe esta propiedad.";
                }
                property = mapper.Map(propertyDto, property);
                await context.SaveChangesAsync();
                return "Registro exitoso.";
            }
            catch (Exception ex)
            {
                return "Ocurrió un error.";
            }
        }

        // Delete this record from the database.
        [HttpDelete("{Id:int}")]
        public async Task<ActionResult<string>> Delete(int Id)
        {
            try
            {
                var property = await context.Properties.FirstOrDefaultAsync(x => x.Id == Id);
                if (property == null)
                {
                    return "No se pudo eliminar, no se encontró esta propiedad.";
                }
                context.Remove(property);
                await context.SaveChangesAsync();
                return "Registro exitoso.";
            }
            catch (Exception ex)
            {
                return "Ocurrió un error.";
            }
        }

        [HttpGet("count")]
        public async Task<ActionResult<DashboardDto>> Count(int Id)
        {
            DashboardDto dashboard = new DashboardDto();
            var owner = context.Owners.AsQueryable();
            dashboard.owner = owner.Count();
            var property = context.Properties.AsQueryable();
            dashboard.property = property.Count();
            var propertyType = context.PropertyTypes.AsQueryable();
            dashboard.propertyType = propertyType.Count();
            return dashboard;
        }
    }
}
