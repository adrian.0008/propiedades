﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PropertiesAPI.Dtos;
using PropertiesAPI.Models;
using PropertiesAPI.Utilities;

namespace PropertiesAPI.Controllers
{
    [ApiController]
    [Route("api/propertyType")]
    public class PropertyTypeController : ControllerBase
    {
        private readonly ILogger<PropertyTypeController> logger;
        private readonly PropertiesContext context;
        private readonly IMapper mapper;

        public PropertyTypeController(
            ILogger<PropertyTypeController> logger,
            PropertiesContext context,
            IMapper mapper)
        {
            this.logger = logger;
            this.context = context;
            this.mapper = mapper;
        }
        // This method loads the list of the requested model by pages.
        [HttpGet]
        public async Task<ActionResult<List<PropertyTypeDto>>> Get([FromQuery] PaginationDTO paginationDTO)
        {
            var queryable = context.PropertyTypes.AsQueryable(); ;
            await HttpContext.InsertPaginationParametersInHeader(queryable);
            var propertyType = await queryable.OrderBy(x => x.Description).Paginate(paginationDTO).ToListAsync();
            return mapper.Map<List<PropertyTypeDto>>(propertyType);
        }
        // This method loads the list of the requested the all property types.
        [HttpGet("loadAll")]
        public async Task<ActionResult<List<PropertyTypeDto>>> LoadAll()
        {
            var propertyType = await context.PropertyTypes.OrderBy(x => x.Description).ToListAsync();
            return mapper.Map<List<PropertyTypeDto>>(propertyType);
        }

        // This method loads all the information of the selected property type.
        [HttpGet("{Id:int}")]
        public async Task<ActionResult<PropertyTypeDto>> Get(int Id)
        {
            var propertyType = await context.PropertyTypes.FirstOrDefaultAsync(x => x.Id == Id);
            if (propertyType == null)
            {
                return NotFound();
            }
            return mapper.Map<PropertyTypeDto>(propertyType);
        }

        // This method saves a new property type.
        [HttpPost]
        public async Task<ActionResult<string>> Post([FromBody] PropertyTypeDto propertyTypeDto)
        {
            try
            {
                var propertyType = mapper.Map<PropertyType>(propertyTypeDto);
                context.Add(propertyType);
                await context.SaveChangesAsync();
                return "Registro exitoso.";
            }
            catch (Exception ex)
            {
                return "Ocurrió un error.";
            }
        }

        // This method edits the selected property type.
        [HttpPut("{Id:int}")]
        public async Task<ActionResult<string>> Put(int Id, [FromBody] PropertyTypeDto propertyTypeDto)
        {
            try
            {
                var propertyType = await context.PropertyTypes.FirstOrDefaultAsync(x => x.Id == Id);
                if (propertyType == null)
                {
                    return "No existe esta tipo de propiedad.";
                }
                propertyType = mapper.Map(propertyTypeDto, propertyType);
                await context.SaveChangesAsync();
                return "Registro exitoso.";
            }
            catch (Exception ex)
            {
                return "Ocurrió un error.";
            }
        }

        // Delete this record from the database.
        [HttpDelete("{Id:int}")]
        public async Task<ActionResult<string>> Delete(int Id)
        {
            try
            {
                var propertyType = await context.PropertyTypes.FirstOrDefaultAsync(x => x.Id == Id);
                if (propertyType == null)
                {
                    return "No se pudo eliminar, no se encontró este tipo de propiedad.";
                }
                context.Remove(propertyType);
                await context.SaveChangesAsync();
                return "Registro exitoso.";
            }
            catch (Exception ex)
            {
                return "Ocurrió un error.";
            }
        }
    }
}
