﻿using Microsoft.EntityFrameworkCore;

namespace PropertiesAPI.Utilities
{
    public static class HttpContextExtensions
    {
        public async static Task InsertPaginationParametersInHeader<T>(this HttpContext httpContext,
            IQueryable<T> queryable)
        {
            if (httpContext == null) { throw new ArgumentNullException(nameof(httpContext)); }

            double cant = await queryable.CountAsync();
            httpContext.Response.Headers.Add("totalNumberOfRecords", cant.ToString());
        }
    }
}
