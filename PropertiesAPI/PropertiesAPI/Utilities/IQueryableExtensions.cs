﻿using PropertiesAPI.Dtos;

namespace PropertiesAPI.Utilities
{
    public static class IQueryableExtensions
    {
        public static IQueryable<T> Paginate<T>(this IQueryable<T> queryable, PaginationDTO paginacionDTO)
        {
            return queryable
                .Skip((paginacionDTO.Page - 1) * paginacionDTO.RecordsByPage)
                .Take(paginacionDTO.RecordsByPage);
        }
    }
}
