﻿using AutoMapper;
using PropertiesAPI.Dtos;
using PropertiesAPI.Models;

namespace PropertiesAPI.Utilities
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<PropertyType, PropertyTypeDto>().ReverseMap();
            CreateMap<Owner, OwnerDto>().ReverseMap();
            CreateMap<Property, PropertyDto>().ReverseMap();
        }
        }
    }
